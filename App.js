'use strict';

// Basic requires
const Path = require('path');
const Hapi = require('hapi');
const Hoek = require('hoek');
const Handlebars = require('handlebars');
const Joi = require('joi');
const Moment = require('moment');
const Sitemap = require('sitemap');

// MongoDB connection
var db = require('monk')('comuneonline:comune2018x@82.165.67.183:1996/comuneonline3?authSource=comuneonline3');

// Nodemailer
var nodemailer = require('nodemailer');
var nodemailerOptions = {
    host: 'localhost',
    port: 25,
    secure: false,
    ignoreTLS: true
};

// Server params
const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
});

server.connection({
    host: '0.0.0.0',
    port: 3000
});

/* 2 LUGLIO 2016 - Ho reso il generatore di sitemap statico. Cioè ho creato la sitemap
/* e la sto servendo staticamente nel route in fondo al file. Mi risparmio grosse rotture di cazzo così.
/* ====
// Sitemap.xml generator
*/
/*
var sm = Sitemap.createSitemap({
    hostname: 'http://comune.online',
    cacheTime: 600000,
    urls: [
        {url: '/', changefreq: 'weekly', priority: 0.8},
        {url: '/informazioni', changefreq: 'weekly', priority: 0.5},
        {url: '/promuovi-ristorante', changefreq: 'weekly', priority: 0.7},
        {url: '/promuovi-locale', changefreq: 'weekly', priority: 0.7},
        {url: '/elenco-pec', changefreq: 'weekly', priority: 0.5},
        {url: '/ricerca-prefisso-telefonico', changefreq: 'weekly', priority: 0.5}
    ]
});

/*
var sitemap_items = db.get('comuni');
sitemap_items.find({}, function(e, docs) {
    for (var i=0; i<docs.length; i++) {
        var sitemap_url = {url: '/comune-di-' + docs[i].slug, changefreq: 'weekly', priority: 0.5};
        var sitemap_url_albopretorio = {url: '/comune-di-' + docs[i].slug + '/albo-pretorio', changefreq: 'weekly', priority: 0.5};
        var sitemap_url_email = {url: '/comune-di-' + docs[i].slug + '/email', changefreq: 'monthly', priority: 0.5};
        var sitemap_url_prefisso = {url: '/prefisso-telefonico-' + docs[i].slug, changefreq: 'monthly', priority: 0.5};
        var sitemap_url_concorsi = {url: '/comune-di-' + docs[i].slug + '/concorsi', changefreq: 'weekly', priority: 0.5};

        //sm.urls.push(sitemap_url);
        //sm.urls.push(sitemap_url_albopretorio);
        //sm.urls.push(sitemap_url_email);
        //sm.urls.push(sitemap_url_prefisso);
        sm.urls.push(sitemap_url_concorsi);
    }
});
*/



// If Comparison Helper for Handlebars
Handlebars.registerHelper('ifCond', function(v1, v2, options) {
    if(v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

// Date Format versione mia
Moment.locale('it');
Handlebars.registerHelper('formatDateAnnuncio', function(date) {
    return Moment(date).format('DD MMMM YYYY - HH:mm');
});


// Templating engine
server.register(require('vision'), (err) => {
    Hoek.assert(!err, err);

server.views({
    engines: {
        html: {
            module: Handlebars,
            isCached: false
        }
    },
    relativeTo: __dirname,
    path: './templates',
    layoutPath: './templates',
    layout: true
});
});

// Static routes
server.register(require('inert'), (err) => {
    if (err) {
        throw err;
    }

    server.route({
    method: 'GET',
    path: '/public/{param*}',
    handler: {
        directory: {
            path: './',
            listing: true
        }
    }
});
});

// ****** //
// Routes //
// ****** //

// Homepage
server.route({
    method: 'GET',
    path: '/',
    handler: function(request, reply) {
        var collection = db.get('comuni');

        collection.find({}, function(e, comuni) {
            var randomComune = comuni[Math.floor(Math.random() * comuni.length)];

            var data = {
                title: 'Comune Online | Informazioni e dati utili, telefoni, email, eventi, news dal tuo comune',
                description: 'Il portale più completo e aggiornato sui Comuni Italiani. Codici, numero di telefono, sito web, indirizzi di posta, links utili.',
                randomComune: randomComune
            };
            return reply.view('bs-home', data);
        });
    }
});

// Lista dei comuni
server.route({
    method: 'GET',
    path: '/comuni',
    handler: function(request, reply) {

        var data = {
            title: 'Lista dei comuni | Comune Online',
            description: 'Lista completa dei comuni inseriti su Comune Online. La lista è in continuo aggiornamento e ampliamento.',
        };

        return reply.view('comuni', data);

    }
});

// Pagina comune singolo - con nuovo template comune2
server.route({
    method: 'GET',
    path: '/comune-di-{comune}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);
        //console.log(comunespacestripped);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Informazioni, numeri utili, indirizzi web, indirizzi di posta, news e altro sul Comune di ' + comune_capitalized,
                comune: comune
            };

            return reply.view('bs-comune', data);
        });
    }
});

// Pagina comune singolo edit
server.route({
    method: 'GET',
    path: '/comune-di-{comune}/edit',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Informazioni, numeri utili, indirizzi web, indirizzi di posta, news e altro sul Comune di ' + comune_capitalized,
                comune: comune
            };

            return reply.view('editcomune', data);
        });
    }
});

server.route({
    method: 'POST',
    path: '/editcomune',
    handler: function(request, reply) {
        var collection = db.get('comuni');

        var nome = request.payload.nome;
        var slug = request.payload.slug;
        var info_intro = request.payload.info_intro;
        var info_telefono1 = request.payload.info_telefono1;
        var info_telefono2 = request.payload.info_telefono2;
        var info_web = request.payload.info_web;
        var info_web_albopretorio = request.payload.info_web_albopretorio;
        var info_email = request.payload.info_email;
        var info_pec = request.payload.info_pec;
        var info_regione = request.payload.info_regione;
        var info_provincia = request.payload.info_provincia;
        var info_sigla = request.payload.info_sigla;
        var info_mappa = request.payload.info_mappa;
        var info_prefissotelefonico = request.payload.info_prefissotelefonico;
        var info_cap = request.payload.info_cap;
        var info_codiceistat = request.payload.info_codiceistat;
        var info_codicecatastale = request.payload.info_codicecatastale;
        var info_superficie = request.payload.info_superficie;
        var info_santopatrono = request.payload.info_santopatrono;
        var info_denominazioneabitanti = request.payload.info_denominazioneabitanti;
        var info_festapatronale = request.payload.info_festapatronale;
        var info_popolazioneresidente = request.payload.info_popolazioneresidente;
        var info_wikipedia = request.payload.info_wikipedia;

        if (nome == '') {
            return reply('Torna indietro e inserisci correttamente i dati.');
        }

        collection.findOne({'slug': slug}, function(e, comune) {
            collection.update(comune._id,
                                {
                                    $set: {
                                        'nome' : nome,
                                        'slug' : slug,
                                        'info_intro' : info_intro,
                                        'info_telefono1' : info_telefono1,
                                        'info_telefono2' : info_telefono2,
                                        'info_web' : info_web,
                                        'info_web_albopretorio': info_web_albopretorio,
                                        'info_email' : info_email,
                                        'info_pec' : info_pec,
                                        'info_regione' : info_regione,
                                        'info_provincia' : info_provincia,
                                        'info_sigla' : info_sigla,
                                        'info_mappa' : info_mappa,
                                        'info_prefissotelefonico': info_prefissotelefonico,
                                        'info_cap' : info_cap,
                                        'info_codiceistat' : info_codiceistat,
                                        'info_codicecatastale' : info_codicecatastale,
                                        'info_superficie' : info_superficie,
                                        'info_santopatrono' : info_santopatrono,
                                        'info_denominazioneabitanti' : info_denominazioneabitanti,
                                        'info_festapatronale' : info_festapatronale,
                                        'info_popolazioneresidente' : info_popolazioneresidente,
                                        'info_wikipedia' : info_wikipedia
                                    }
                                },
                                function(err) {
                                    if (err) return reply('Errore durante update.');
                                });
        });

        return reply.redirect('/comune-di-' + slug);
    }
});

// Pagina ricerca comune
server.route({
    method: 'GET',
    path: '/cerca',
    handler: function(request, reply) {
        var searchkey = encodeURIComponent(request.query.key);

        if (searchkey.length == 0) {
            return reply.redirect('/');
        }

        //console.log(searchkey);

        var searchquery = {
            $or: [
                {
                    nome: {
                        $regex: searchkey,
                        $options: 'i'
                    }
                }
            ]
        };

        var collection = db.get('comuni');

        collection.find(searchquery, {}, function(e, comuni) {
            var data = {
                title: 'Risultati di ricerca | Comune Online',
                description: 'Ricerca comune nel database di Comune Online inserendo un testo libero.',
                comuni: comuni
            };

            return reply.view('risultatiricercacomuni', data);
        });
    }
});


// Pagina albo pretorio
server.route({
    method: 'GET',
    path: '/comune-di-{comune}/albo-pretorio',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Albo Pretorio Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Albo Pretorio del Comune di ' + comune_capitalized + '. Atti amministrativi, delibere, determinazioni comunali, matrimoni.',
                comune: comune
            };

            return reply.view('albopretorio', data);
        });
    }
});

// Pagina concorsi
server.route({
    method: 'GET',
    path: '/comune-di-{comune}/concorsi',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Concorsi Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Tutti i concorsi pubblici del comune di ' + comune_capitalized + '. Scadenze, requisiti e come partecipare.',
                comune: comune
            };

            return reply.view('concorsi', data);
        });
    }
});

// Pagina email
server.route({
    method: 'GET',
    path: '/comune-di-{comune}/email',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Indirizzi Email Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'In questa pagina trovi gli indirizzi di posta elettronica per contattare il Comune di ' + comune_capitalized + '.',
                comune: comune
            };

            return reply.view('email', data);
        });
    }
});

// Pagina mappa
server.route({
    method: 'GET',
    path: '/comune-di-{comune}/mappa',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Mappa interattiva del Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Visualizza la mappa del Comune di ' + comune_capitalized + '.',
                comune: comune
            };

            return reply.view('mappa', data);
        });
    }
});

// Pagina prefisso telefonico
server.route({
    method: 'GET',
    path: '/prefisso-telefonico-{comune}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('comuni');

        collection.findOne({'slug': comunespacestripped}, function(e, comune) {
            var data = {
                title: 'Prefissi telefonici del Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Prefissi telefonici del Comune di ' + comune_capitalized + '.',
                comune: comune
            };

            return reply.view('prefissotelefonico', data);
        });
    }
});

// Pagina fotografie
server.route({
    method: 'GET',
    path: '/fotografie-{comune}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);

        var collection = db.get('foto');

        collection.find({'comune': comune}, function(e, fotografie) {
            var data = {
                title: 'Fotografie Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Raccolta di materiale fotografico del Comune di ' + comune_capitalized + '.',
                comune: comune,
                foto: fotografie
            };

            return reply.view('fotografie', data);
        });
    }
});

// Pagina ristoranti
server.route({
    method: 'GET',
    path: '/ristoranti-{comune}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var comunespacestripped = decodeURI(comune);

        var collection = db.get('ristoranti');

        collection.find({'comune': comunespacestripped}, function(e, ristoranti) {
            var data = {
                title: 'Ristoranti del Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Elenco dei migliori ristoranti del Comune di ' + comune_capitalized + '.',
                comune: comunespacestripped,
                ristoranti: ristoranti
            };

            return reply.view('ristoranti', data);
        });
    }
});

// Pagina ristorante singolo
server.route({
    method: 'GET',
    path: '/ristorante/{slug}',
    handler: function(request, reply) {
        var slug = encodeURIComponent(request.params.slug);

        var collection = db.get('ristoranti');

        collection.findOne({'slug': slug}, function(e, ristorante) {
            var data = {
                title: ristorante.nome + ' | Comune Online',
                description: 'Pagina ristoranti. ' + ristorante.nome + '.',
                ristorante: ristorante
            };

            return reply.view('ristorante', data);
        });
    }
});

server.route({
    method: 'GET',
    path: '/foto/{slug}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var slug = encodeURIComponent(request.params.slug);

        var collection = db.get('foto');

        collection.findOne({'slug': slug}, function(e, fotografia) {
            var data = {
                title: fotografia.titolo + ' | Comune Online',
                description: 'Raccolta di materiale fotografico del Comune di ' + comune_capitalized + '.',
                comune: comune,
                foto: fotografia
            };

            return reply.view('fotografia', data);
        });
    }
});

// Elenco news comune
server.route({
    method: 'GET',
    path: '/news/{comune}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);

        var collection = db.get('news');

        collection.find({'comune': comune}, function(e, news) {
            var data = {
                title: 'Tutte le news dal Comune di ' + comune_capitalized + ' | Comune Online',
                description: 'Elenco delle ultime news istituzionali direttamente dal Comune di ' + comune_capitalized + '.',
                comune: comune,
                news: news
            };

            return reply.view('newscomune', data);
        });
    }
});

// News singolo comune
server.route({
    method: 'GET',
    path: '/allnews',
    handler: function(request, reply) {
        var collection = db.get('scrapednews');

        collection.find({}, function(e, items) {
            var data = {
                title: 'Comune Online | Tutte le news',
                description: 'Le news da tutti i Comuni Italiani',
                news: items
            };

            return reply.view('allnews', data);
        });
    }
});

// News singolo comune
server.route({
    method: 'GET',
    path: '/news/{comune}/{slug}',
    handler: function(request, reply) {
        var comune = encodeURIComponent(request.params.comune);
        var comune_capitalized = comune[0].toUpperCase() + comune.slice(1);
        var slug = encodeURIComponent(request.params.slug);

        var collection = db.get('news');

        collection.findOne({'comune': comune, 'slug': slug}, function(e, item) {
            var data = {
                title: item.titolo + ' | ' + comune_capitalized + ' | Comune Online',
                description: item.titolo,
                comune: comune,
                item: item
            };

            return reply.view('newscomunesingola', data);
        });
    }
});

server.route({
    method: 'GET',
    path: '/informazioni',
    handler: function(request, reply) {
        var data = {
            title: 'Informazioni sul sito | Comune Online',
            description: 'Informazioni sul sito Comune Online, grande database di dati su ogni singolo comune italiano. In continua espansione.'
        };

        return reply.view('informazioni', data);
    }
});

// *******
// Elenchi
// *******
// Elenco tutte le PEC
server.route({
    method: 'GET',
    path: '/elenco-pec',
    handler: function(request, reply) {
        var collection = db.get('comuni');

        collection.find({}, {sort: {'nome': 1}},function(e, comuni) {
            var data = {
                title: 'Elenco PEC Comuni - Posta Elettronica Certificata',
                description: 'Elenco degli indirizzi di Posta Elettronica Certificata (PEC) dei Comuni Italiani.',
                comuni: comuni
            };

            return reply.view('elencopec', data);
        });
    }
});


// *****
// Forms
// *****

// Aggiungi Attività
server.route({
    method: 'GET',
    path: '/aggiungi-attivita',
    handler: function(request, reply) {
        var data = {
            title: 'Aggiungi la tua attività sul sito | Comune Online',
            description: 'Se hai un ristorante, un pub, sei un commercialista, avvocato o hai una qualunque attività inseriscila gratuitamente su Comune Online.'
        };

        return reply.view('aggiungiattivita', data);
    }
});

// Form: Locale
server.route({
    method: 'GET',
    path: '/promuovi-locale',
    handler: function(request, reply) {
        var data = {
            title: 'Promuovi il tuo locale | Comune Online',
            description: 'Se hai un ristorante, un pub, una discoteca o qualunque altro tipo di locale inseriscilo gratuitamente nel database di Comune Online.'
        };

        return reply.view('formlocale', data);
    }
});

server.route({
    method: 'POST',
    path: '/aggiungi-attivita',
    handler: function(request, reply) {
        var input_nome = request.payload.input_nome;
        var input_indirizzo = request.payload.input_indirizzo;
        var input_comune = request.payload.input_comune;
        var input_telefono = request.payload.input_telefono;
        var input_descrizione = request.payload.input_descrizione;

        if (input_nome == '') {
            return reply('Torna indietro e inserisci correttamente i dati nel modulo.');
        }

        // send mail
        var transporter = nodemailer.createTransport(nodemailerOptions);
        var mailOptions = {
            from: 'info@reflow.studio', // sender address
            to: 'dalis84@gmail.com', // list of receivers
            subject: 'Richiesta inserimento attivita su Comune Online', // Subject line
            text: 'Nome: ' + input_nome + '\nIndirizzo: ' + input_indirizzo + '\nComune: ' + input_comune + '\nTelefono: ' + input_telefono + '\nDescrizione: ' + input_descrizione
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                return console.log(error);
            }
            //console.log('Messaggio inviato: ' + info.response);
        });

        // response
        var data = {
            title: 'Modulo inviato | Comune Online',
            description: 'Grazie per aver inviato il modulo.'
        };

        return reply.view('moduloinviato', data);
    }
});

// Form: Ristorante
server.route({
    method: 'GET',
    path: '/promuovi-ristorante',
    handler: function(request, reply) {
        var data = {
            title: 'Promuovi il tuo ristorante | Comune Online',
            description: 'Se hai un ristorante, inseriscilo gratuitamente nella directory di Comune Online.'
        };

        return reply.view('formristorante', data);
    }
});

// Form: Ricerca prefisso
server.route({
    method: 'GET',
    path: '/ricerca-prefisso-telefonico',
    handler: function(request, reply) {
        var prefisso = encodeURIComponent(request.params.prefisso);

        var data = {
            title: 'Ricerca per Prefisso Telefonico | Comune Online',
            description: 'Cerca un comune a partire dal prefisso telefonico. Oppure trova il prefisso telefonico del comune cercato.'
        };

        return reply.view('ricercaprefisso', data);
    }
});

server.route({
    method: 'POST',
    path: '/ricerca-prefisso-telefonico',
    handler: function(request, reply) {
        var prefisso = request.payload.prefisso;

        var collection = db.get('comuni');

        collection.find({'info_prefissotelefonico': prefisso}, function(e, comuni) {

            var data = {
                title: 'Ricerca per Prefisso Telefonico | Comune Online',
                description: 'Cerca un comune a partire dal prefisso telefonico. Oppure trova il prefisso telefonico del comune cercato.',
                prefisso: prefisso,
                comuni: comuni
            };

            return reply.view('ricercaprefissorisultati', data);
        });
    }
});



// Sitemap URL
// Sitemap Route

server.route({
    method: 'GET',
    path: '/sitemapold.xml',
    handler: function(request, reply) {
        var response = reply(sm.toString());
        response.type('application/xml');
    }
});


// Questo è il nuovo handler di sitemap. È finalmente statico, così non c'è bisogno di ricaricare tutto il db ogni volta.
server.route({
    method: 'GET',
    path: '/sitemap.xml',
    handler: function(request, reply) {
        reply.file('./sitemap.xml');
    }
});
server.route({
    method: 'GET',
    path: '/sitemap01.xml',
    handler: function(request, reply) {
        reply.file('./sitemap01.xml');
    }
});
server.route({
    method: 'GET',
    path: '/sitemap02.xml',
    handler: function(request, reply) {
        reply.file('./sitemap02.xml');
    }
});

// Server start
server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at: ', server.info.uri);
})